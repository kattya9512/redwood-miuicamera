
Clone MiuiCamera.apk & MiuiEditor.apk to _common/proprietary/system/priv-app/MiuiCamera_ & _common/proprietary/system/priv-app/MiuiEditor_ respectively


Link for both : https://drive.google.com/drive/folders/1lrBkZGOLyEr0cdDnJrvKElugnPvtCoUI?usp=sharing


# Call the Leica Camera setup
$(call inherit-product-if-exists, vendor/xiaomi/redwood-miuicamera/products/miuicamera.mk)


# Inherit from proprietary files for Leica Camera
-include vendor/xiaomi/redwood-miuicamera/products/board.mk


